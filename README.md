# SQI datasets of sEMG signals in deglutition

Documentation for SQI datasets of sEMG signals in deglutition. This repository contains the dataset generated in the paper accesible thout DOI: http://--------

:microscope:

![Visitors](https://visitor-badge.glitch.me/badge?page_id=Domadrona.sqi-datasets-of-semg-signals-in-deglutition?color=green)

![Repo size](https://img.shields.io/gitlab/repo-size/Domadrona/sqi-dataset-of-semg-signals-in-deglutition/sqi-datasets-of-semg-signals-in-deglutition?color=green&style=plastic)

![](https://img.shields.io/twitter/follow/julio_jimmy?style=social)
![](https://img.shields.io/github/followers/Domadrona?style=social)

## Contents

- [How to use this repository](#how-to-use-this-guide)
- [Requirements](#requirements)
- [How to cite](#how-to-cite)
- [License](#license)


## How to use this repository
## Requirements
1. Python

2. Sklearn 2.23

3. etc
## How to cite
If you use this dataset, and if you want to cite this work you can use this:

```bibtex
@article{author-name,
  author       = {authors names},
  title        = {Automatic detection of poor quality signals as a pre-processing scheme in the analysis of sEMG signals in swallowing},
  month        = ---,
  year         = 2021,
  publisher    = {----},
  version      = {----},
  doi          = {----},
  url          = {https://doi.org/-----}
}
```
## License
